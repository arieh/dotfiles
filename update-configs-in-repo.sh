#!/usr/bin/env bash
configFiles=(
	~/.zshrc
	~/.vimrc
	~/.tmux.conf
	~/.config/kitty/kitty.conf
)

for file in ${configFiles[@]}; 
do
	cp ${file} .
done
