# What is this?

dotfiles, here's a link in case you don't know

[what are dotfiles?](https://medium.com/@webprolific/getting-started-with-dotfiles-43c3602fd789)

# Extra fixes

alsa-fix to stop audio pulsing in weird manner ripped from [this stack exchange post](https://askubuntu.com/questions/1226630/static-pulsing-sound-produced-from-internal-microphones)

install sed in order to use
