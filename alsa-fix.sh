grep -nP "(Element Capture)|(Element \w+ Mic)|(Element Mic)" /usr/share/pulseaudio/alsa-mixer/paths/analog-input*.conf | while IFS='' read -r LINE || [ -n "${LINE}" ]; do

    IFS=':' read -r -a array <<< "${LINE}"
    path="${array[0]}";
    num="${array[1]}";
    element="${array[2]}";


    for i in $(seq 5 $END); do

        param="$(sed -n "$((num + i))p" < "$path")"
        if [[ $param == "volume ="* ]]; then
            echo "processing line $path:$((num + i))";
            echo "replacing <$param> with <volume = zero>";
            sed -i "$((num + i))s/.*/volume = zero/" "$path";
        fi
    done

done
